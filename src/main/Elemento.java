package main;

public class Elemento {
	String nome;
	Boolean completo;
	
	public Elemento (String nome, Boolean completo){
		this.nome = nome;
		this.completo = completo;
	}
	
	public String toString(){
		return nome + " " + complejoString() + " \n" ;
				
		
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Boolean getCompleto() {
		return completo;
	}
	
	public String complejoString() {
		if (this.completo){
			return " ☑️ ";
		}else{
			return "";
		}
	}
	
	public void setCompleto(Boolean completo) {
		this.completo = completo;
	}
	
}
