package main;

import java.util.ArrayList;
import java.util.Arrays;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;


public class EjemploBot extends TelegramLongPollingBot  {
	ArrayList<Elemento> lista = new ArrayList();
	 
	
	private void doVer(Long chat){
		 Integer cont = 0;
		 SendMessage mens = new SendMessage().setText("");
		 mens.setChatId(chat);
		 while(cont < lista.size()){
			 System.out.print(cont.toString());
			 mens.setText( mens.getText() + cont.toString() +".- " + lista.get(cont).toString());
			 cont++;
		 }
		 try {
	            execute(mens); // Call method to send the message
	        } catch (TelegramApiException e) {
	            e.printStackTrace();
	        }
	}
	
	private void doRematar(ArrayList<String> tarea,Long chat){
	
		Integer numTarea = new Integer(tarea.get(1)); 
		try{
			lista.get(numTarea).setCompleto(true);
		}catch(IndexOutOfBoundsException e){
			SendMessage mens = new SendMessage().setText("Tarea no existente");
			try {
	            execute(mens); // Call method to send the message
	            doVer(chat);
	        } catch (TelegramApiException m) {
	            m.printStackTrace();
	        }
		}		
	}
	
	private void doHelp (Long chat){
		
	}
	
	private void doEngadir(String tarea ,Long chat){
		System.out.println(tarea);
		
		lista.add(new Elemento(tarea.replaceFirst("/engadir", "") ,false));
		doVer(chat);
	}
	
	
	@Override
    public void onUpdateReceived(Update update) {
		if (update.hasMessage() && update.getMessage().hasText()) {
			String mess = update.getMessage().getText();
			System.out.println(update.getMessage().getText());
			ArrayList<String> mens= new ArrayList<String>(Arrays.asList(mess.split( " ")));
			Long chat = update.getMessage().getChatId();
			switch (mens.get(0).toLowerCase()) {
				case "/ver": 
					 doVer(chat);
					break;
				case "/rematada": 
					doRematar(mens,chat);
					break;
				case "/engadir": 
					doEngadir(mess,chat);
					break;
				
			}
		}
	}

    @Override
    public String getBotUsername() {
        // TODO
        return "testEloy1bot";
    }

    
    @Override
    public String getBotToken() {
        // TODO
    	System.out.println("Working Directory = " +
                System.getProperty("user.dir"));
    	
    	return "550835065:AAFXMNQd2iagGfAf7PzoJrEX-MdxmuRHUqo";
    }
}

